# The harvester. 

Given a configuration file which defines the local harvest repository 
and a set of sites providing data, iterate over the sites:

- get the last version of the resources file (local copy) and build a list
of the current view of the repo.
- download the updated resources.xml file to find out what the site is publishing
- iterate over the records in the resources list
- compare the hash of the new file to the hash from the last update
 - if different - download a new version
 - remove the file from the old list
- iterate over the list of differences and remove any old files that
 are no longer in the providers set. 
 #TODO: How to handle this ?

## Configuration
 
See the example in harvest.cfg.

## Invocation

    ./harvest.py --config harvest.cfg

For more verbosity re: what the code is doing add --info.
For even more ... add --debug

If you want to harvest a single site only:
    ./harvest.py --config harvest.cfg --site {Site short code in config file}
