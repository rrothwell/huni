#!/usr/bin/env python

from lxml import etree
import os
import os.path
import sys
import argparse
import ConfigParser
import logging
import hashlib
import requests
import subprocess
import copy
import re
from datetime import datetime, timedelta

class counter:
    def __init__(self, total):
        self.last = 0
        self.total = total
        if total < 1000:
            self.logat = 50
        elif total < 100000:
            self.logat = 1000
        else:
            self.logat = 10000

    def update(self, current):
        if current > (self.last + self.logat):
            self.last += self.logat
            try:
                process_time = time.time() - self.last_time
                log.info("Processed %s of %s... (%s records in %1.2f seconds)" % (self.last, self.total, self.logat, float(process_time)))
            except:
                log.info("Processed %s of %s..." % (self.last, self.total))

class Harvest:
    def __init__(self, cfg, resync):
        # read in the config file
        self._ingest(cfg, resync)

    def _ingest(self, cfg, resync):
        """Read the config file - set var's as instance var's"""
        config = ConfigParser.SafeConfigParser()
        config_file = config.read(cfg)
        if not config_file:
            log.error("Does that config file exist? %s" % cfg)
            sys.exit()

        self.datasets = {}
        for section in config.sections():
            data = config.items(section)
            for param, value in data:

                if section == 'Datasets':
                    try:
                        (code, resource_type, frequency, resource) = value.split(',')
                        self.datasets[code.strip()] = { 
                            'resource': resource.strip(), 
                            'type': resource_type.strip(), 
                            'frequency': frequency.strip() 
                        }
                    except ValueError:
                        log.error("Looks like a datasource is incorrect. You provided '%s'" % value)
                        log.error("Where it should be like 'Set2: EOAS, simple, weekly, http://www.findandconnect.gov.au/act/eac/resources.xml'")
                        sys.exit()

                else:
                    setattr(self, param, value)
                    log.debug("cfg: %s: %s" % (param, value))

        for d in self.datasets:
            log.debug("cfg: datasets: %s" % d)
        for site, resources in self.datasets.items():
            if not os.path.exists(os.path.join(self.repository, site)):
                os.mkdir(os.path.join(self.repository, site))

        # ensure the repo exists
        if not os.path.exists(self.repository):
            os.makedirs(self.repository)

        # special opts to be coerced to boolean
        if args.resync is not None:
            self.resync = True
        else:
            self.resync = config.getboolean('General', 'resync')

    def get_hash(self, fname):
        """Given a filename, return the md5 hash"""
        with open(fname, 'r') as fh:
            try:
                m = hashlib.md5()
                data = fh.read()
                m.update(data)
                return m.hexdigest()
            except:
                log.error('Error in the has function :: OHRM code')
                log.debug(sys.exc_info())

    def retrieve(self, site, resource, local_file):
        """Retrieve a web resource and save it as a local file

        @params:
        resource: a URL to a file for download
        local_file: the full qualified path of the local file
            to save the data to
        """
        log.debug("Downloading %s" % resource)
        try:
            resp = requests.get(resource) 
        except requests.exceptions.ConnectionError:
            log.error("Connection to %s broken." % site)
            sys.exit()
            
        if resp.status_code == 200:
            try:
                fh = open(local_file, 'w')
                fh.write(resp.content)
                fh.close()
            except UnicodeEncodeError:
                log.error("Broken resource %s" % resource)
                log.error(resp.content)

        elif resp.status_code == 404:
            log.error("Couldn't retrieve file %s" % resource)

    def update(self, process_only):
        """Update the local harvest

        @params:
        process_only: if not None, harvest only this site

        Given the datasets in the config file, iterate over them and
        update the local copies of each as required.
        """

        for site, provider in self.datasets.items():
            if process_only is not None and process_only != site:
                log.debug("Skipping %s; looking for %s" % (site, process_only))
                continue

            site_repo = os.path.join(self.repository, site)
            update_file = os.path.join(self.repository, "%s%s" % (site, '-resources.xml'))

            log.debug("Resource file: %s" % provider['resource'])
            log.debug("Local resources file: %s" % update_file)

            # is it an OAI endpoint or one the more web2 ones...
            if provider['type'] == 'OAI':
                if provider['frequency'] == 'daily':
                    # date now - one day to produce: &from=2013-06-13T07:56:44z
                    seconds = 86400
                elif provider['frequency'] == 'weekly':
                    # date now - one week to produce: &from=2013-06-13T07:56:44z
                    seconds = 604800
                else:
                    log.error("You specified a frequency of %s but I don't know what to do with that" % provider['frequency'])
                    log.error("Re-read the docs in the config file and fix it.")
                    continue

                if not self.resync:
                    tfrom = datetime.now() - timedelta(0, seconds)
                    tfrom = '&from=%s' % tfrom.strftime("%Y-%m-%dT%H:%M:%Sz")
                else:
                    tfrom = ''
                resource = provider['resource'] 
                self.process_oai(site, resource, tfrom, site_repo, update_file)

            elif provider['type'] == 'simple':
                self.process_simple(site, provider['resource'], site_repo, update_file)

            else:
                log.error("Unknown type '{0}' specified for {1}. Your options are: 'simple' or 'OAI'.".format(resource['type'], site))

    def process_oai(self, site, resource, tfrom, site_repo, update_file, token=None):
        #
        log.debug("Looking for updated resources: %s " % resource)

        if token is not None:
            get_url = re.sub('\?verb=.*', "?verb=ListIdentifiers&resumptionToken=" + token, resource)
            #resource = resource.replace('?verb=.*', "?verb=ListIdentifiers&resumptionToken=%s" % token)
            token = None
        else:
            get_url = resource + tfrom

        log.info("Updating the local data for %s" % site)
        log.info("Getting: %s" % get_url)

        ## get the updated resources file
        try:
            resp = requests.get(get_url)

            if resp.status_code == 200:
                fh = open(update_file, 'w')
                fh.write(resp.content)
                fh.close()
            else:
                log.error("Couldn't get the file list for %s - 404 not found. Skipping site." % site)                                                          
                return
        except requests.exceptions.ConnectionError:
            log.error("Couldn't get the resources file for %s. Skipping site." % site)
            return


        ## first we need to get the resumptionToken - if it exists
        #   http://www.openarchives.org/OAI/2.0/guidelines-harvester.htm#resumptionToken
        for event, element in etree.iterparse(copy.deepcopy(update_file), tag='{http://www.openarchives.org/OAI/2.0/}resumptionToken'):
            token = element.text
            element.clear()

        ## first we need a count of total records
        total = 0
        for event, element in etree.iterparse(copy.deepcopy(update_file), tag='{http://www.openarchives.org/OAI/2.0/}header'):
            total += 1

            element.clear()
        log.info("Total records in set: %s" % total)

        i = 0
        c = counter(total)


        ## then, for every 'record' in our current resources file
        for event, element in etree.iterparse(update_file, tag='{http://www.openarchives.org/OAI/2.0/}header'):
            i += 1
            c.update(i)

            # figure out the entity_name and datestamp
            entity_name = element[0].text
            entity_datestamp = element[0].getnext().text

            #  see if we already have that file - compare the sums - download if different
            site_repo = os.path.join(self.repository, site)
            provider_datafile = resource.replace('ListIdentifiers', 'GetRecord') + "&identifier=%s" % entity_name

            # and clean out the nasties in the name that will screw up our filesystem
            entity_name = entity_name.replace(':', '-').replace('/', '_')
            entity_local_datafile = os.path.join(site_repo, entity_name)
            log.debug("Local file: %s" % entity_local_datafile)
            log.debug("Provider datafile %s" % provider_datafile)

            # the MAGIC: get the file
            self.retrieve(site, provider_datafile, entity_local_datafile)

            # ditch the element so we save memory - we no longer need it
            element.clear()

        # we found a resumption token so back to the top and do it all again
        if token is not None:
            self.process_oai(site, resource, tfrom, site_repo, update_file, token=token)


    def process_simple(self, site, resource, site_repo, update_file):
        # get the data file then pass it to etree to walk
        provider_base = os.path.split(resource)[0]
        log.debug("Site base: %s" % provider_base)
        log.info("Updating the local data for %s, %s" % (site, resource))

        ## create the hash of existing files
        #log.info("Constructing the list of files in the local %s repo." % site)
        old_set = {}
        try:
            for event, element in etree.iterparse(update_file, tag='record'):
                # figure out the entity_name and hash
                entity_name = element.xpath('name')[0].text
                entity_hash = element.xpath('hash')[0].text
                old_set[entity_name] = entity_hash
        except IOError:
            # non existent file
            log.error("I can't find %s" % update_file)
            return
        except etree.XMLSyntaxError:
            # empty file
            log.error("I think %s is a bit screwey" % update_file)
            return

        ## get the updated resources file
        try:
            resp = requests.get(resource)
                 
            if resp.status_code == 200:
                # if the resources file is gzipped - unzip it and store it unzipped
                if resource.endswith('.gz'):
                    # get it, then save it to tmp file so gzip can deal with it
                    zipped_file = update_file + ".gz"
                    fh = open(zipped_file, 'wb')
                    fh.write(resp.content)
                    fh.close()
                    cmd = [ "/bin/gunzip", "--force", zipped_file ]
                    try:
                        returncode = subprocess.check_call(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    except subprocess.CalledProcessError:
                        log.error("Not sure if the resources file from %s is in order. Skipping site." % provider_base)
                        return
                else:
                    fh = open(update_file, 'w')
                    fh.write(resp.content)
                    fh.close()

            else:
                log.error("Couldn't get resources.xml file for %s - 404 not found. Skipping site." % site)
                return
        except requests.exceptions.ConnectionError:
            log.error("Couldn't get the resources file for %s. Skipping site." % site)
            return

        ## first we need a count of total records
        total = 0
        for event, element in etree.iterparse(copy.deepcopy(update_file), tag='record'):
            total += 1
            element.clear()
        log.info("Total records in set: %s" % total)

        i = 0
        c = counter(total)
        ## then, for every 'record' in our current resources file
        for event, element in etree.iterparse(update_file, tag='record'):
            i += 1
            c.update(i)

            # figure out the entity_name and hash
            entity_name = element.xpath('name')[0].text
            entity_hash = element.xpath('hash')[0].text

            #  see if we already have that file - compare the sums - download if different
            site_repo = os.path.join(self.repository, site)
            entity_local_datafile = os.path.join(site_repo, entity_name)
            provider_datafile = os.path.join(provider_base, entity_name)

            # no local copy of file so just get it
            #if not os.path.exists(entity_local_datafile):
            #    log.debug("New data %s" % entity_local_datafile)
            #    self.retrieve(site, provider_datafile, entity_local_datafile)

            # local copy by that name already down
            #else:
            # compare the old hash to the new one - download if different
            if self.resync:
                self.retrieve(site, provider_datafile, entity_local_datafile)
            elif old_set.has_key(entity_name) and entity_hash != old_set[entity_name]:
                self.retrieve(site, rovider_datafile, entity_local_datafile)
                del old_set[entity_name]

            # ditch the element so we save memory - we no longer need it
            element.clear()

if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='Harvest the dataset feeds')
    parser.add_argument('--config', required=True, dest='cfg',
        help="Configuration file.")
    parser.add_argument('--site', dest='site', default=None, help='Sometimes you just want to harvest a single site')
    parser.add_argument('--resync', dest='resync', action='store_true', default=None, \
        help='Sometimes you want to reharvest from scratch.\
        This option tells the harvester to ignore diffs and last harvest time and start from scratch. It \
    overrides whatever you have set for that option in the config file.')

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)


    # quieten the requests library
    requests_log = logging.getLogger("requests")
    requests_log.setLevel(logging.ERROR)

    log = logging.getLogger('HARVESTER')

    h = Harvest(args.cfg, args.resync)
    h.update(args.site)