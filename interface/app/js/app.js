'use strict';


// Declare app level module which depends on filters, and services
angular.module('huni', ['ui.bootstrap', 'huni.services', 'huni.filters']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/search',  {
        templateUrl: 'partials/search.html',  
        controller: 'SearchController',
    });
    $routeProvider.when('/search/:query', {
        templateUrl: 'partials/search.html',  
        controller: 'SearchController'
    });
    $routeProvider.when('/browse',  {
        templateUrl: 'partials/browse.html',  
        controller: 'BrowseController'
    });
    $routeProvider.when('/publications',  {
        templateUrl: 'partials/publications.html',  
        controller: 'PublicationController'
    });
    $routeProvider.when('/publication/:uuid', {
        templateUrl: 'partials/publication_view.html',
        controller: 'PublicationController'
    });
    $routeProvider.when('/myhuni',  {
        templateUrl: 'partials/myhuni.html',  
    });
    $routeProvider.when('/help',    {
        templateUrl: 'partials/help.html'
    });
    $routeProvider.when('/syntax',    {
        templateUrl: 'partials/syntax.html'
    });
    $routeProvider.when('/links',    {
        templateUrl: 'partials/links.html'
    });
    $routeProvider.when('/about',   {
        templateUrl: 'partials/about.html'
    });
    $routeProvider.when('/results', {
        templateUrl: 'partials/results.html', 
        controller: 'ResultsController'
    });
    $routeProvider.when('/network/:record', {
        templateUrl: 'partials/network.html', 
        controller: 'NetworkController'
    });
    $routeProvider.when('/login',   {
        templateUrl: 'partials/login.html',
        controller: 'LoginController'
    });
    $routeProvider.when('/author/:name', {
        templateUrl: 'partials/author_publication_list.html',
        controller: 'AuthorPublicationController'
    });
    $routeProvider.otherwise({redirectTo: '/search'});
  }]);
