/* 
 *   AuthorPublicationController 
*/
function AuthorPublicationController($scope, $routeParams, $http, $location, contentFormatService) {
    $scope.author = $routeParams.name

    $scope.init = function() {
        var config = {};
        config.params = {
            'author': $scope.author
        }
        // get the Publication data if public
        $http.get('/vlab/author', config)
            .success(function(response) {   
                var publications = response.publications;
                for (var i = 0; i < publications.length ; i++) {
                    publications[i].synopsis = contentFormatService.formatIt(publications[i].synopsis);
                }
                $scope.data = response.publications;
                //console.log($scope.data);
            })
            .error(function(response) {
                // unauthorized
                $scope.data = [];
            });
    }


}
AuthorPublicationController.$inject = ['$scope', '$routeParams', '$http', '$location', 'ContentFormatService'];

