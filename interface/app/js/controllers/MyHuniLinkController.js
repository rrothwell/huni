
/*
 * MyHuniLinkController
 */
function MyHuniLinkController($rootScope, $scope, $http, $location, $timeout) {
    $scope.init = function() {
        $scope.detail_view = false;

        // populate the Links list
        $scope.get();

    }

    // get Link list
    $scope.get = function() {
        $http.get('/vlab/mylink')
            .success(function(response) {
                $scope.links = response.links;
            });
    }


    // get the details of an existing Link
    $scope.read = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        // the Link data
        $http.get('/vlab/mylink', config)
            .success(function(response) {
                $scope.data = response.link;
                $scope.data.host = $location.host();
                $scope.detail_view = true;
            })
            .error(function(response) {
                $scope.error_msg = 'Unable to get link data'; 
            });
    }

    // get member data
    $scope.getMemberData = function() {
        $scope.records = [];

        var members = $scope.data.members;
        for (var i = 0; i < members.length ; i++) {
            solrSearchService.getRecordData(members[i])
                .then(function(response) { 
                    var record_data = response; 
                    $scope.records.push(cleanRecord(record_data));
                    //console.log($scope.records);
                });
        }
    }

    // handle Link update
    $scope.update = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.put('/vlab/mylink', { 'data': $scope.data }, config)
            .then(function(response) {
                var t = $timeout(function() { $scope.success_msg = undefined; }, 3000);
                $scope.success_msg = 'Link updated';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to update link data';
            });
    }

/*    // handle Link removal
    $scope.delete = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.delete('/vlab/myLink', config)
            .then(function(response) {
                var t = $timeout(function() { 
                    $scope.success_msg = undefined; 
                    $scope.get();
                    $scope.data = undefined;
                    $scope.detail_view = false;
                }, 3000);
                $scope.success_msg = 'Link deleted';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to delete Link';
            });
    }
 */

}
MyHuniLinkController.$inject = [ '$rootScope', '$scope', '$http', '$location', '$timeout'];

