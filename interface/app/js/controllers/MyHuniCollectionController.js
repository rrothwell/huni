
/*
 * MyHuniCollectionController
 */
function MyHuniCollectionController($rootScope, $scope, $http, $location, $timeout, solrSearchService) {
    $scope.init = function() {
        $scope.detail_view = false;

        // populate the collections list
        $scope.get();

    }

    // get collection list
    $scope.get = function() {
        $http.get('/vlab/mycollection')
            .success(function(response) {
                $scope.collections = response.collections;
            });
    }


    // get the details of an exisiting collection
    $scope.read = function(uuid) {
        $scope.primary = true;

        var config = {};
        config.params = {
            'uuid': uuid
        };
        // the collection data
        $http.get('/vlab/mycollection', config)
            .success(function(response) {
                $scope.data = response.collection;
                $scope.data.host = $location.host();
                $scope.detail_view = true;
                $scope.getMemberData();
            })
            .error(function(response) {
                $scope.error_msg = 'Unable to get collection data'; 
            });
    }

    // get member data
    $scope.getMemberData = function() {
        $scope.records = [];

        var members = $scope.data.members;
        for (var i = 0; i < members.length ; i++) {
            solrSearchService.getRecordData(members[i])
                .then(function(response) { 
                    var record_data = response; 
                    $scope.records.push(cleanRecord(record_data));
                    //console.log($scope.records);
                });
        }
    }

    // handle collection update
    $scope.update = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.put('/vlab/mycollection', { 'data': $scope.data }, config)
            .then(function(response) {
                var t = $timeout(function() { $scope.success_msg = undefined; }, 3000);
                $scope.success_msg = 'Collection updated';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to update collection data';
            });
    }

    // publish the article
    $scope.publish = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.put('/vlab/mypublication', { 'data': $scope.data }, config)
            .then(function(response) {
                var t = $timeout(function() { $scope.success_msg = undefined; }, 3000);
                $scope.success_msg = 'Article Published';
                $rootScope.$broadcast('update publications');
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to publish the article at this time';
            });
    }

    // handle collection removal
    $scope.delete = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.delete('/vlab/mycollection', config)
            .then(function(response) {
                var t = $timeout(function() { 
                    $scope.success_msg = undefined; 
                    $scope.get();
                    $scope.data = undefined;
                    $scope.detail_view = false;
                }, 3000);
                $scope.success_msg = 'Collection deleted';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to delete collection';
            });
    }

    // handle record removal
    $scope.deleteRecord = function(record_name, collection_id) {
        console.log('delete record');
        var config = {};
        config.params = {
            'uuid': collection_id,
            'record_name': record_name
        };
        $http.delete('/vlab/mycollection', config)
            .then(function(response) {
                var t = $timeout(function() { 
                    $scope.success_msg = undefined; 
                    $scope.read(collection_id);
                }, 3000);
                $scope.success_msg = 'Record deleted';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to delete record';
            });
    }

    var cleanRecord = function(record) {
        record = record[0];
        for (elem in record) {
            switch (elem) {
                case '_version_': delete record[elem]; 
                case 'prov_site_address': delete record[elem]; 
                case 'prov_site_long': delete record[elem]; 
                case 'prov_site_short': delete record[elem]; 
                case 'prov_site_tag': delete record[elem]; 
                case 'prov_doc_last_update': delete record[elem];
                //case 'prov_source': record[elem.replace('_', ' ')] = record[elem]; delete record[elem];
            }
        }
        
        // return it 
        return record;
    } 
}
MyHuniCollectionController.$inject = [ '$rootScope', '$scope', '$http', '$location', '$timeout', 'SolrSearchService'];

