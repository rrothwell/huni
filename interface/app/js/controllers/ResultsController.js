/*
 *   ResultsController 
*/
function ResultsController($scope, $location, $anchorScroll, $http, 
    $timeout, solrSearchService, huniOntology) {

    // initialise the controller
    $scope.init = function() {
        // initialisation
        $scope.providers_search = [];
        $scope.types_search = [];
        $scope.applied_type_filters = [];
        $scope.applied_provider_filters = [];
        $scope.documentsToSave = [];

        // initialises linked tech state
        $scope.resetConnection();

        // do the initial data load
        $scope.results = solrSearchService.getResultSet();
        $scope.query = solrSearchService.getQuery();

        // set the result pane width to span8 initially
        $scope.controls = true;

        // this is effectively a page reload
        if ($scope.query === undefined) {
            $location.path('/');
        }

        // the list of fields to query
        $scope.query_fields = [ 'text', 'text_rev' ];

        try {
            constructQuery();
        } catch (TypeError) {
            $location.path('/');
        }
    };

    // Handle search result updates
    $scope.$on('searchResultsUpdated', function() {
        $scope.results = solrSearchService.getResultSet();
        $scope.query = solrSearchService.getQuery();
        addConnectionData();
        //$location.hash('top');
        //$anchorScroll();
        //console.log($scope.search);
    });

    // Handle facet updates
    $scope.$on('facetResultsUpdated', function() {
        var facets = solrSearchService.getFacetResults();
        //console.log(facets);

        // filter out the applied types
        var t = setSelected($scope.applied_type_filters, facets.type);
        $scope.types = t;
        var p = setSelected($scope.applied_provider_filters, facets.prov_site_long);
        $scope.providers = p;
    });

    /*
     * If a filter has been applied, set it's selected flag to true
     *
     * For example: 
     * $scope.applied_type_filters = [ 'Artefact' ]
     *
     * For facets.type.facet === 'Artefact'
     *   set facets.type.selected === true
     *
     */
    var setSelected = function(applied_filters, data) {
        // with no filters applied, return the original set
        if (applied_filters == undefined || applied_filters.length == 0) {
            return data.sort()
        }

        // see if any of the facet results are in the applied list
        // if so - set their selected flag to true
        for (var i = 0 ; i < data.length ; i ++) {
            if (applied_filters.indexOf(data[i].facet) !== -1) {
                data[i].selected = true;
            }
        }
        return data.sort();
    }

    var addConnectionData = function() {
        var docs = $scope.results.docs;
        for (var i = 0; i < docs.length; i++) {
            var docid = docs[i].docid;
            
            // get publication counts
            var config = {};
            config.params = {
                'name': docid
            }
            // populate the pub count for each entity
            $http.get('/vlab/entity', config)
                .then(function(response) {
                    var docid = response.config.params['name'];
                    for (var i=0; i < $scope.results.docs.length; i++) {
                        if ($scope.results.docs[i].docid === docid) {
                            $scope.results.docs[i].pub_count = response.data.publications.length;
                        }
                    }
                });
            // populate the link count - src and tgt - for each entity
            $http.get('/vlab/link', config)
                .then(function(response) {
                    var docid = response.config.params['name'];
                    for (var i=0; i < $scope.results.docs.length; i++) {
                        if ($scope.results.docs[i].docid === docid) {
                            $scope.results.docs[i].src_link_count = response.data.src_links.length;
                            $scope.results.docs[i].tgt_link_count = response.data.tgt_links.length;
                        }
                    }
                });
        }
    }

    $scope.toggleProvenance = function() {
        $scope.show_provenance = ! $scope.show_provenance;
    }

    // handle the assertion of a connection
    $scope.addConnection = function(record_id, type) {
        var uncheck = false;
        for (var i = 0 ; i < $scope.connection_set.length; i++) {
            if ($scope.connection_set[i] === record_id) {
                $scope.connection_set.splice(i, 1);
                uncheck = true;
            } 
        }
        // if the incoming id is in the set, then we need to remove it
        // this is the checkbox getting unchecked
        if (uncheck) {
            // unset whichever it was that was unchecked
            if ($scope.src_entity === record_id){
                $scope.src_entity = null;
                $scope.src_type = null;
            }
            if ($scope.tgt_entity === record_id){
                $scope.tgt_entity = null;
                $scope.tgt_type = null;
            }
            // modify the button label accordingly
            if ($scope.src_entity !== null && $scope.tgt_entity === null) {
                $scope.connection_button_label = "Tag target entity for connection";
            } else {
                $scope.connection_button_label = "Tag source entity for connection";
            }

        // when src == null, set it
        } else if ($scope.src_entity === null) {
            $scope.src_entity = record_id;
            $scope.src_type = type;
            $scope.connection_button_label = "Tag target entity for connection";
            $scope.controls = false;
            $scope.connection_set.push(record_id);
        // when tgt == null, set it
        } else if ($scope.tgt_entity === null) {
            $scope.tgt_entity = record_id;
            $scope.tgt_type = type;
            $scope.connection_button_label = "You can now assert the connection";
            $scope.connection_set.push(record_id);
        }
    }

    $scope.resetConnection = function() {
        try {
            for (var i = 0; i < $scope.connection_set.length; i++) {
                var e = $scope.connection_set[i] + "_connection";
                try {
                    document.getElementById(e).checked = false;
                } catch (e) {
                    // no need to do anything, it's a selection that is no longer
                    //  on the page. Consider the user has probably done a few
                    //  searches and is adding as they go.
                }
            
            }
        } catch (e) { 
            //do nothing
            //  being lazy and using this method to init the state 
            //   so connection_set is undefined on init
        }
        $scope.connection_set = [];
        $scope.src_entity = null;
        $scope.src_type = null;
        $scope.tgt_entity = null;
        $scope.tgt_type = null;
        $scope.assertion = '';
        $scope.connection_button_label = "Tag source entity for connection";
    }

    $scope.createConnection = function() {
        var data = {
            'src_entity': $scope.src_entity,
            'src_type': $scope.src_type,
            'tgt_entity': $scope.tgt_entity,
            'tgt_type': $scope.tgt_type,
            'assertion': $scope.assertion
        }
        $http.post('/vlab/mylink', data)
            .then(function(response) {
                var t = $timeout(function() { 
                    $scope.link_success_msg = undefined; 
                    $scope.resetConnection();
                    addConnectionData();
                }, 1500);
                $scope.link_success_msg = 'Connection created';
            },
            function(response) {
                var t = $timeout(function() { $scope.link_error_msg = undefined; }, 3000);
                $scope.link_error_msg = 'Unable to create connection';
            });
    }

    // handle record addition to collection
    $scope.addToCollection = function(record_id, type) {
        // iterate over the stack - if we find a match remove it and break
        // else if we get to the end - add the new member
        console.log('before');
        console.log($scope.documentsToSave);
        var found = false;
        for (var i = 0; i < $scope.documentsToSave.length ; i ++) {
            if ($scope.documentsToSave[0].record === record_id) {
                $scope.documentsToSave.splice(i, 1);
                found = true;
                break;
            }
        }
        // it wasn't already in - so add it
        if (found === false) { 
            $scope.documentsToSave.push({
                'record': record_id,
                'type': type
            });
        }
        console.log('after');
        console.log($scope.documentsToSave);

        if ($scope.documentsToSave.length > 0) {
            $scope.controls = false;
        } else {
            $scope.controls = true;
        }
    };

    // autocomplete on collection names
    $scope.autocompleteCollections = function(collection_name) {
        var config = {};
        config.params = {
            'name': collection_name
        };
        return $http.get('/vlab/ac/collection', config)
            .then(function(response) { 
                //console.log(response.data.collections);
                return response.data.collections; 
            });
    }

    // autocomplete on search names
    $scope.autocompleteSearch = function(search_name) {
        var config = {};
        config.params = {
            'name': search_name
        };
        return $http.get('/vlab/ac/search', config)
            .then(function(response) {
                //console.log(response.data);
                return response.data.searches;
            });
    }

    // save the records to the collection; in the process
    //  creating it if it doesn't exist
    $scope.saveToCollection = function() {
        $scope.collection_success_msg = undefined;
        $scope.collection_error_msg = undefined;
        data = {
            'collection_name': $scope.collection_name,
            'collection_members': $scope.documentsToSave
        };
        $http.post('/vlab/mycollection', data)
            .then(function(response) {
                //console.log($scope.documentsToSave);
                // uncheck all the checked checkboxes
                for (var i = 0; i < $scope.documentsToSave.length; i++) {
                    var e = $scope.documentsToSave[i] + "_collection";
                    try {
                        document.getElementById(e).checked = false;
                    } catch (e) {
                        // no need to do anything, it's a selection that is no longer
                        //  on the page. Consider the user has probably done a few
                        //  searches and is adding as they go.
                    }

                }
                // wipe the saved list
                $scope.documentsToSave = [];
                // wipe the collection name
                $scope.collection_name = null;

                var t = $timeout(function() { $scope.collection_success_msg = undefined; }, 1500);
                $scope.collection_success_msg = 'Collection saved';
            },
            function(response) {
                var t = $timeout(function() { $scope.collection_error_msg = undefined; }, 3000);
                $scope.collection_error_msg = 'Unable to save collection';
            });
    }

    // save the search
    $scope.saveSearch = function() {
        $scope.save_success_msg = undefined;
        $scope.save_error_msg = undefined;
        data = {
            'search_name': $scope.search_name,
            'query': $scope.query,
            'overwrite': $scope.overwrite_search 
        }
        $http.post('/vlab/mysearch', data)
            .then(function(response) {
                // wipe the search_name field
                $scope.search_name = ''
                var t = $timeout(function() { $scope.save_success_msg = undefined; }, 1500);
                $scope.save_success_msg = 'Search saved';
            },
            function(response) {
                var t = $timeout(function() { $scope.save_error_msg = undefined; }, 3000);
                if (response.status === 400) {
                    $scope.save_error_msg = 'Save failed; name already in use';
                }
            });
    }

    // get next page of results
    $scope.nextPage = function() {
        $scope.query.start = solrSearchService.NextPage($scope.results.start, $scope.results.total);
        //console.log("next: " + $scope.query.start);
        solrSearchService.doit($scope.query);
    };
    
    // get previous page of results
    $scope.previousPage = function() {
        $scope.query.start = solrSearchService.PreviousPage($scope.results.start);
        //console.log("previous: " + $scope.query.start);
        solrSearchService.doit($scope.query);
    };

    $scope.search = function() {
        //console.log('Results controller, search method');
        constructQuery();
    }

    // Add / remove a type filter from the result set
    //  filter needs to be: { field: 'foo', data: 'bar'' }
    $scope.typeFilter = function(filter) {
        // update the result set
        $scope.update($scope.types_search, $scope.applied_type_filters, filter);
    }

    // Add / remove a data provider filter from the result set
    //  filter needs to be: { field: 'foo', data: 'bar'' }
    $scope.providerFilter = function(filter) {
        // update the result set
        $scope.update($scope.providers_search, $scope.applied_provider_filters, filter);
    }

    $scope.update = function(which, applied, filter) {
        if (filter.value === true) {
            // add the provider if not already there
            which = pushStack(which, filter.data);
            applied = pushStack(applied, filter.data);
        } else {
            // remove the provider if it's in the list
            which = popStack(which, filter.data);
            applied = popStack(applied, filter.data);
        }
        constructQuery();
    }

    var pushStack = function(stack, data) {
        if (stack.indexOf(data) === -1) {
            stack.push(data);
        }
        return stack;
    }
    var popStack = function(stack, data) {
        var field_pos = stack.indexOf(data);
        if (field_pos !== -1) {
            stack.splice(field_pos,1);
        }
        return stack;
    }

    var constructQuery = function() {
        // if user_query comes in blank - set it to *
        if ($scope.query.user_query === '') { 
            $scope.query.user_query = '*';
        }
        if ($scope.query.user_query === undefined) {
            $scope.query.user_query = '*'
        }

        // our query placeholder as we're putting it all together
        var query = [];
        
        // sort out any type filters
        if ($scope.types_search.length > 0) {
            var new_query = [];
            for (var i = 0 ; i < $scope.types_search.length ; i++) {
                new_query.push({ field: 'type', data: $scope.types_search[i] });
            }
            query.push(new_query);
        }

        // sort out any data provider filters
        if ($scope.providers_search.length > 0) {
            var new_query = [];
            for (var i = 0 ; i < $scope.providers_search.length ; i++) {
                new_query.push({ field: 'prov_site_long', data: $scope.providers_search[i] });
            }
            query.push(new_query);
        }

        // sort out the query fields
        var new_query = [];
        for (var n = 0; n < $scope.query_fields.length; n++) {
            var fieldname = $scope.query_fields[n];
            new_query.push({ field: fieldname, data: $scope.query.user_query });
        }   
        query.push(new_query);

        // update the search results
        $scope.query.query = query;
        $scope.query.start = 0;
        solrSearchService.doit($scope.query);

        // update the facet counts
        var query = solrSearchService.getQuery();
        q = solrSearchService.AssembleQuery(query);
        solrSearchService.faceton(['prov_site_long', 'type'], q);

    }

}
ResultsController.$inject = ['$scope', '$location', '$anchorScroll', '$http', 
    '$timeout', 'SolrSearchService', 'HuniOntology'];

