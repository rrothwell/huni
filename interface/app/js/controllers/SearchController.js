/* 
 *   SearchController 
*/
function SearchController($rootScope, $scope, $location, $routeParams,
    solrSearchService, huniOntology, userMgtService) {

    $scope.$on('facetResultsUpdated', function() {
        var facets = solrSearchService.getFacetResults();
        //console.log(facets);
        $scope.providers = facets.prov_site_long;
        $scope.nproviders = $scope.providers.length;
        $scope.entity_types = facets.type;
        $scope.nentity_types = $scope.entity_types.length;
    });

    $scope.$on('totalDocumentsUpdated', function() {
        $scope.ndocuments = solrSearchService.getTotalDocuments();
    });

    // init - get list and total, available document count
    $scope.init = function() {
        solrSearchService.faceton(['prov_site_long', 'type']);        
        solrSearchService.documentsInIndex();
        if ($routeParams.query !== undefined) {
            $scope.search($routeParams.query);
        }
    }

    // search
    $scope.search = function(query) {

        var search_request = {
            start: '0',
            result_fields: '*'
        };

        //console.log("User search: " + $scope.query);
        if ($scope.query === undefined) {
            // the user didn't type anything so search *
            $scope.query = '*';
        };
        if (query !== undefined) {
            $scope.query = query;
        }

        // when searching, the user has no concept of entity type available
        //  so, we have to effectively search all relevant fields for
        //  every entity type... /sigh
        //  
        //  Thankfully - copyfield gives us only two to look in
        var q = [];
        q.push({field: 'text', data: $scope.query});
        q.push({field: 'text_rev', data: $scope.query});
        search_request.query = [] 
        search_request.query.push(q);
        
        // and finally, put in the query string;
        search_request.user_query = $scope.query;

        //console.log("Search query:");
        //console.log(search_request);
        solrSearchService.doit(search_request);
        
        // send us to the new view then do the search
        $location.path('/results');
    }

}
SearchController.$inject = ['$rootScope', '$scope', '$location', '$routeParams',
    'SolrSearchService', 'HuniOntology', 'UserMgtService' ];

