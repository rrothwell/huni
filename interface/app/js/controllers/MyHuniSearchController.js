/*
 * MyHuniSearchController
 */
function MyHuniSearchController($scope, $http, $timeout) {
    $scope.init = function() {
        $scope.detail_view = false;
       //populate the saved searches list
        $scope.get();
    }

    $scope.get = function() {
        $http.get('/vlab/mysearch')
            .success(function(response) {
                //console.log($scope.saved_searches);
                $scope.searches = response.searches;
            });
    }

    // read a record
    $scope.read = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.get('/vlab/mysearch', config)
            .success(function(response) {
                response.searches.query = pretty_print(response.searches.query);
                $scope.data = response.searches;
                $scope.detail_view = true;
            });
    }

    // handle updates to search notes
    $scope.update = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };

        $http.put('/vlab/mysearch', { 'data': $scope.data }, config)
            .then(function(data) {
                var t = $timeout(function() { $scope.success_msg = undefined; }, 1500);
                $scope.success_msg = 'Search updated';
            },
            function(data, status) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to update search data';
            });
    }

    // handle deleting a saved search
    $scope.delete = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.delete('/vlab/mysearch', config)
            .then(function(response) {

                var t = $timeout(function() { 
                    $scope.success_msg = undefined; 
                    $scope.get();
                    $scope.data = undefined;
                    $scope.detail_view = false;
                }, 1500);
                $scope.success_msg = 'Search deleted';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to delete search';
            });
    }

    var pretty_print = function(query) {
        //query = angular.fromJson(query);
        var pretty_query = '';
        var user_query = query.user_query;
        var query = query.query;
        //console.log(query);
        for (var i = 0; i < query.length; i++) {
            var terms = query[i];
            for (var j = 0; j < terms.length ; j++) {
                field = terms[j].field;
                data = terms[j].data;
                if (field !== 'text_rev') {
                    if (i == (query.length - 1)) {
                        pretty_query += field + ' = "' + data + '"';
                    } else {
                        pretty_query += field + ' = "' + data + '" AND ';
                    }
                }
            }
        }
        //console.log(pretty_query);
        return pretty_query;
    }
}
MyHuniSearchController.$inject = ['$scope', '$http', '$timeout'];
