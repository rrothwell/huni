/* 
 *   LoginStatusController 
*/
function LoginStatusController($scope, userMgtService, $http, $location) {


    // init - get list and total, available document count
    $scope.login = function() {
        $scope.logged_in == false;
        
        userMgtService.IsLoggedIn()
            .then(
                // success callback
                function(data) {
                    $scope.logged_in = data.isLoggedIn;
                    $scope.username = data.username;
                    $scope.vlab_down = false;
                    if ($scope.logged_in !== true) {
                        // not logged in so do something sensible
                        switch($location.path()) {
                            case '/myhuni':
                                $location.path('/search');
                            default:
                                // do nothing at all - it's openaccess
                        }
                    }
                },

                // error callback
                function(http_return_code) {
                    $scope.vlab_down = true;
                }
            );
    }

    // logout handler
    $scope.logout = function() {
        $scope.logged_in = false;
        $http.get('/vlab/logout');
        if ($location.path() === '/myhuni') {
            $location.path('/search');
        }
    }
}
LoginStatusController.$inject = ['$scope', 'UserMgtService', '$http', '$location'];

