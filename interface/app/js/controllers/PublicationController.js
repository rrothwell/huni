/* 
 *   PublicationController 
*/
function PublicationController($scope, $routeParams, $http, $location, 
    solrSearchService, contentFormatService) {

    $scope.init = function() {
        // get the Publication data if public
        var config = {};
        config.params = {
            'uuid': $routeParams.uuid
        }
        $http.get('/vlab/publication', config)
            .success(function(response) {   
                console.log(response);
                try {
                    response.publication.synopsis = contentFormatService.formatIt(response.publication.synopsis);
                } catch (e) {
                    // skip the error - no synopsis added
                }
                try {
                    response.publication.notes = contentFormatService.formatIt(response.publication.notes);
                } catch (e) {
                    // skip the error - no notes added
                }
                $scope.data = response.publication;
                $scope.data.host = $location.host();
                $scope.data.url = $location.absUrl();
                console.log($scope.data);
                $scope.getMemberData();
            })
            .error(function(response) {
                // unauthorized
            });
    }

    // get member data
    $scope.getMemberData = function() {
        $scope.records = [];
        var members = $scope.data.members;
        for (var i = 0; i < members.length ; i++) {
            solrSearchService.getRecordData(members[i])
                .then(function(response) {
                    var data = response[0];
                    $scope.records.push({ 'source': data.prov_source, 'id': data.docid });
                    //console.log($scope.records);
                });
        }
    }

    // get list of publications available - populates
    //  the publications page
    $scope.getPublicationList = function() {
        $http.get('/vlab/publication')
            .then(function(response) {
                $scope.data = response.data.publications;
            });
    }
}
PublicationController.$inject = ['$scope', '$routeParams', '$http', '$location', 
    'SolrSearchService', 'ContentFormatService'];

