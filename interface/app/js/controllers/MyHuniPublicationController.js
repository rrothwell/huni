/* 
 *   MyHuniPublicationController 
*/
function MyHuniPublicationController($scope, $http, $location, $timeout, contentFormatService) {

    $scope.$on('update publications', function() {
        $scope.get();
    });
    $scope.init = function() {
        // get the publication list on load
        $scope.get();
    }

    $scope.delete = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.delete('/vlab/mypublication', config)
            .success(function(response) {
                // this refreshes the publication list
                $scope.get();

                var t = $timeout(function() { $scope.success_msg = undefined; }, 3000);
                $scope.success_msg = 'Publication deleted';
                
            })
            .error(function(response) {
                var t = $timeout(function() { $scope.success_msg = undefined; }, 3000);
                $scope.success_msg = 'Unable to delete publication';
            });

    }

    // get the current users list of publications
    $scope.get = function() {
        $http.get('/vlab/mypublication')
            .success(function(response) {   
                var publications = response.publications;
                for (var i = 0; i < publications.length ; i++) {
                    publications[i].synopsis = contentFormatService.formatIt(publications[i].synopsis);
                }
                $scope.data = publications;
                $scope.data.host = $location.host();
                $scope.data.url = $location.absUrl();
                //console.log($scope.data);
            })
            .error(function(response) {
                // unauthorized
                $location.path('/search');
            });
    }

}
MyHuniPublicationController.$inject = ['$scope', '$http', '$location', '$timeout', 'ContentFormatService'];

