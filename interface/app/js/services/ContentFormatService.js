/* 
* ContentFormatService
*/
services.service('ContentFormatService', 
    [ function() {

    var ContentFormatService;

    var self = this;

    self.formatIt = function(data) {
        if (data.length === 0) {
            return data;
        }

        marked.setOptions({
          gfm: true,
          highlight: function (code, lang, callback) {
            pygmentize({ lang: lang, format: 'html' }, code, function (err, result) {
              callback(err, result.toString());
            });
          },
          tables: true,
          breaks: false,
          pedantic: false,
          sanitize: true,
          smartLists: true,
          smartypants: false,
          langPrefix: 'lang-'
        });
        return marked(data);
    }

    return ContentFormatService;
}]);
