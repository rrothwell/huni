/* 
* UserMgtService
*/
services.service('UserMgtService', 
    [ '$rootScope', '$http', '$q', function($rootScope, $http, $q) {

    var UserMgtService;

    var self = this;

    //var VLAB_URL = 'http://huni.esrc.unimelb.edu.au/vlab';
    var VLAB_URL = 'http://dev01.esrc.info/vlab';

    // return the current result set
    self.isLoggedIn = function() {
        return self.is_logged_in;
    }

    /* IsLoggedIn
     * 
     * Ask the vlab if the user is logged in
     *
     */
    self.IsLoggedIn = function() {
        var check_login = VLAB_URL + '/isloggedin';

        // do it
        var deferred = $q.defer();
        $http.get(check_login)
            .success(function(data, http_return_code) {
                // save the data
                deferred.resolve(data);
            })
            .error(function(data, http_return_code) {
                deferred.reject(http_return_code);
            });
        return deferred.promise;
    }

    return UserMgtService;
}]);
