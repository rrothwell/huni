/* 
* HuniOntology
*/
services.service('HuniOntology', 
    [ '$rootScope', '$http', function($rootScope, $http) {

    var HuniOntology;

    var self = this;

    self.entities = {
        'Artefact': {
            'search': [ 'name', 'abstract' ],
        },
        'Bibliography': {
            'search': [ 'title', 'subtitle', 'description', 'synopsis' ],
        },
        'Concept': {
            'search': [ 'name', 'abstract' ],
        },
        'Company': {
            'search': [ 'name', 'address' ],
        },
        'Corporate Body': {
            'search': [ 'name', 'abstract' ],
        },
        'Event': {
            'search': [ 'name', 'abstract' ],
        },
        'Film': {
            'search': [ 'title', 'description' ],
        },
        'Organisation': {
            'search': [ 'name', 'abstract' ],
        },
        'Person': {
            'search': [ 'given_name', 'family_name', 'occupation', 'abstract' ],
        },
        'Place': {
            'search': [ 'name', 'abstract' ],
        },
        'Production': {
            'search': [ 'release_title', 'alternative_title', 'description', 'synopsis' ],
        },
        'Venue': {
            'search': [ 'name', 'address', 'suburb' ],
        },
        'default': {
            'search': [ 'given_name', 'family_name', 'occupation', 'name', 
                        'abstract', 'address', 'description', 'synopsis',
                        'title', 'release_title'
            ],
        }
    };

    self.getResultFields = function(entity) {
        entity = typeof entity !== 'undefined' ? entity : 'default';
        return self.entities[entity].result;
    }

    self.getQueryFields = function(entity) {
        entity = typeof entity !== 'undefined' ? entity : 'default';
        return self.entities[entity].search;
    }

    self.getEntities = function() {
        var entities = [];
        for (var elem in self.entities) {
            if (elem !== 'default') {
                entities.push(elem);
            };
        }
        return entities;
    }

    return HuniOntology;
}]);
