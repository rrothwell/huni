/* Controllers */
'use strict';/* 
 *   AuthorPublicationController 
*/
function AuthorPublicationController($scope, $routeParams, $http, $location, contentFormatService) {
    $scope.author = $routeParams.name

    $scope.init = function() {
        var config = {};
        config.params = {
            'author': $scope.author
        }
        // get the Publication data if public
        $http.get('/vlab/author', config)
            .success(function(response) {   
                var publications = response.publications;
                for (var i = 0; i < publications.length ; i++) {
                    publications[i].synopsis = contentFormatService.formatIt(publications[i].synopsis);
                }
                $scope.data = response.publications;
                //console.log($scope.data);
            })
            .error(function(response) {
                // unauthorized
                $scope.data = [];
            });
    }


}
AuthorPublicationController.$inject = ['$scope', '$routeParams', '$http', '$location', 'ContentFormatService'];

/* 
 *   BrowseController 
*/
function BrowseController($rootScope, $scope, $loc) {
}
BrowseController.$inject = ['$rootScope', '$scope', '$location'];

/* 
 *   LoginController 
*/
function LoginController($rootScope, $scope, $http) {

    $scope.init = function() {
        $scope.providers_visible = true;
    }

    var opts = {
      lines: 15, // The number of lines to draw
      length: 16, // The length of each line
      width: 3, // The line thickness
      radius: 12, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: 'auto', // Top position relative to parent in px
      left: 'auto' // Left position relative to parent in px
    };

    // init - get list and total, available document count
    $scope.login = function(provider) {
        $scope.providers_visible = false;
        var target = document.getElementById('spinner');
        var spinner = new Spinner(opts).spin(target);
    }


}
LoginController.$inject = ['$rootScope', '$scope', '$http'];

/* 
 *   LoginStatusController 
*/
function LoginStatusController($scope, userMgtService, $http, $location) {


    // init - get list and total, available document count
    $scope.login = function() {
        $scope.logged_in == false;
        
        userMgtService.IsLoggedIn()
            .then(
                // success callback
                function(data) {
                    $scope.logged_in = data.isLoggedIn;
                    $scope.username = data.username;
                    $scope.vlab_down = false;
                    if ($scope.logged_in !== true) {
                        // not logged in so do something sensible
                        switch($location.path()) {
                            case '/myhuni':
                                $location.path('/search');
                            default:
                                // do nothing at all - it's openaccess
                        }
                    }
                },

                // error callback
                function(http_return_code) {
                    $scope.vlab_down = true;
                }
            );
    }

    // logout handler
    $scope.logout = function() {
        $scope.logged_in = false;
        $http.get('/vlab/logout');
        if ($location.path() === '/myhuni') {
            $location.path('/search');
        }
    }
}
LoginStatusController.$inject = ['$scope', 'UserMgtService', '$http', '$location'];


/*
 * MyHuniCollectionController
 */
function MyHuniCollectionController($rootScope, $scope, $http, $location, $timeout, solrSearchService) {
    $scope.init = function() {
        $scope.detail_view = false;

        // populate the collections list
        $scope.get();

    }

    // get collection list
    $scope.get = function() {
        $http.get('/vlab/mycollection')
            .success(function(response) {
                $scope.collections = response.collections;
            });
    }


    // get the details of an exisiting collection
    $scope.read = function(uuid) {
        $scope.primary = true;

        var config = {};
        config.params = {
            'uuid': uuid
        };
        // the collection data
        $http.get('/vlab/mycollection', config)
            .success(function(response) {
                $scope.data = response.collection;
                $scope.data.host = $location.host();
                $scope.detail_view = true;
                $scope.getMemberData();
            })
            .error(function(response) {
                $scope.error_msg = 'Unable to get collection data'; 
            });
    }

    // get member data
    $scope.getMemberData = function() {
        $scope.records = [];

        var members = $scope.data.members;
        for (var i = 0; i < members.length ; i++) {
            solrSearchService.getRecordData(members[i])
                .then(function(response) { 
                    var record_data = response; 
                    $scope.records.push(cleanRecord(record_data));
                    //console.log($scope.records);
                });
        }
    }

    // handle collection update
    $scope.update = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.put('/vlab/mycollection', { 'data': $scope.data }, config)
            .then(function(response) {
                var t = $timeout(function() { $scope.success_msg = undefined; }, 3000);
                $scope.success_msg = 'Collection updated';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to update collection data';
            });
    }

    // publish the article
    $scope.publish = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.put('/vlab/mypublication', { 'data': $scope.data }, config)
            .then(function(response) {
                var t = $timeout(function() { $scope.success_msg = undefined; }, 3000);
                $scope.success_msg = 'Article Published';
                $rootScope.$broadcast('update publications');
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to publish the article at this time';
            });
    }

    // handle collection removal
    $scope.delete = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.delete('/vlab/mycollection', config)
            .then(function(response) {
                var t = $timeout(function() { 
                    $scope.success_msg = undefined; 
                    $scope.get();
                    $scope.data = undefined;
                    $scope.detail_view = false;
                }, 3000);
                $scope.success_msg = 'Collection deleted';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to delete collection';
            });
    }

    // handle record removal
    $scope.deleteRecord = function(record_name, collection_id) {
        console.log('delete record');
        var config = {};
        config.params = {
            'uuid': collection_id,
            'record_name': record_name
        };
        $http.delete('/vlab/mycollection', config)
            .then(function(response) {
                var t = $timeout(function() { 
                    $scope.success_msg = undefined; 
                    $scope.read(collection_id);
                }, 3000);
                $scope.success_msg = 'Record deleted';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to delete record';
            });
    }

    var cleanRecord = function(record) {
        record = record[0];
        for (elem in record) {
            switch (elem) {
                case '_version_': delete record[elem]; 
                case 'prov_site_address': delete record[elem]; 
                case 'prov_site_long': delete record[elem]; 
                case 'prov_site_short': delete record[elem]; 
                case 'prov_site_tag': delete record[elem]; 
                case 'prov_doc_last_update': delete record[elem];
                //case 'prov_source': record[elem.replace('_', ' ')] = record[elem]; delete record[elem];
            }
        }
        
        // return it 
        return record;
    } 
}
MyHuniCollectionController.$inject = [ '$rootScope', '$scope', '$http', '$location', '$timeout', 'SolrSearchService'];


/*
 * MyHuniLinkController
 */
function MyHuniLinkController($rootScope, $scope, $http, $location, $timeout) {
    $scope.init = function() {
        $scope.detail_view = false;

        // populate the Links list
        $scope.get();

    }

    // get Link list
    $scope.get = function() {
        $http.get('/vlab/mylink')
            .success(function(response) {
                $scope.links = response.links;
            });
    }


    // get the details of an existing Link
    $scope.read = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        // the Link data
        $http.get('/vlab/mylink', config)
            .success(function(response) {
                $scope.data = response.link;
                $scope.data.host = $location.host();
                $scope.detail_view = true;
            })
            .error(function(response) {
                $scope.error_msg = 'Unable to get link data'; 
            });
    }

    // get member data
    $scope.getMemberData = function() {
        $scope.records = [];

        var members = $scope.data.members;
        for (var i = 0; i < members.length ; i++) {
            solrSearchService.getRecordData(members[i])
                .then(function(response) { 
                    var record_data = response; 
                    $scope.records.push(cleanRecord(record_data));
                    //console.log($scope.records);
                });
        }
    }

    // handle Link update
    $scope.update = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.put('/vlab/mylink', { 'data': $scope.data }, config)
            .then(function(response) {
                var t = $timeout(function() { $scope.success_msg = undefined; }, 3000);
                $scope.success_msg = 'Link updated';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to update link data';
            });
    }

/*    // handle Link removal
    $scope.delete = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.delete('/vlab/myLink', config)
            .then(function(response) {
                var t = $timeout(function() { 
                    $scope.success_msg = undefined; 
                    $scope.get();
                    $scope.data = undefined;
                    $scope.detail_view = false;
                }, 3000);
                $scope.success_msg = 'Link deleted';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to delete Link';
            });
    }
 */

}
MyHuniLinkController.$inject = [ '$rootScope', '$scope', '$http', '$location', '$timeout'];

/* 
 *   MyHuniPublicationController 
*/
function MyHuniPublicationController($scope, $http, $location, $timeout, contentFormatService) {

    $scope.$on('update publications', function() {
        $scope.get();
    });
    $scope.init = function() {
        // get the publication list on load
        $scope.get();
    }

    $scope.delete = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.delete('/vlab/mypublication', config)
            .success(function(response) {
                // this refreshes the publication list
                $scope.get();

                var t = $timeout(function() { $scope.success_msg = undefined; }, 3000);
                $scope.success_msg = 'Publication deleted';
                
            })
            .error(function(response) {
                var t = $timeout(function() { $scope.success_msg = undefined; }, 3000);
                $scope.success_msg = 'Unable to delete publication';
            });

    }

    // get the current users list of publications
    $scope.get = function() {
        $http.get('/vlab/mypublication')
            .success(function(response) {   
                var publications = response.publications;
                for (var i = 0; i < publications.length ; i++) {
                    publications[i].synopsis = contentFormatService.formatIt(publications[i].synopsis);
                }
                $scope.data = publications;
                $scope.data.host = $location.host();
                $scope.data.url = $location.absUrl();
                //console.log($scope.data);
            })
            .error(function(response) {
                // unauthorized
                $location.path('/search');
            });
    }

}
MyHuniPublicationController.$inject = ['$scope', '$http', '$location', '$timeout', 'ContentFormatService'];

/*
 * MyHuniSearchController
 */
function MyHuniSearchController($scope, $http, $timeout) {
    $scope.init = function() {
        $scope.detail_view = false;
       //populate the saved searches list
        $scope.get();
    }

    $scope.get = function() {
        $http.get('/vlab/mysearch')
            .success(function(response) {
                //console.log($scope.saved_searches);
                $scope.searches = response.searches;
            });
    }

    // read a record
    $scope.read = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.get('/vlab/mysearch', config)
            .success(function(response) {
                response.searches.query = pretty_print(response.searches.query);
                $scope.data = response.searches;
                $scope.detail_view = true;
            });
    }

    // handle updates to search notes
    $scope.update = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };

        $http.put('/vlab/mysearch', { 'data': $scope.data }, config)
            .then(function(data) {
                var t = $timeout(function() { $scope.success_msg = undefined; }, 1500);
                $scope.success_msg = 'Search updated';
            },
            function(data, status) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to update search data';
            });
    }

    // handle deleting a saved search
    $scope.delete = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.delete('/vlab/mysearch', config)
            .then(function(response) {

                var t = $timeout(function() { 
                    $scope.success_msg = undefined; 
                    $scope.get();
                    $scope.data = undefined;
                    $scope.detail_view = false;
                }, 1500);
                $scope.success_msg = 'Search deleted';
            },
            function(response) {
                var t = $timeout(function() { $scope.error_msg = undefined; }, 3000);
                $scope.error_msg = 'Unable to delete search';
            });
    }

    var pretty_print = function(query) {
        //query = angular.fromJson(query);
        var pretty_query = '';
        var user_query = query.user_query;
        var query = query.query;
        //console.log(query);
        for (var i = 0; i < query.length; i++) {
            var terms = query[i];
            for (var j = 0; j < terms.length ; j++) {
                field = terms[j].field;
                data = terms[j].data;
                if (field !== 'text_rev') {
                    if (i == (query.length - 1)) {
                        pretty_query += field + ' = "' + data + '"';
                    } else {
                        pretty_query += field + ' = "' + data + '" AND ';
                    }
                }
            }
        }
        //console.log(pretty_query);
        return pretty_query;
    }
}
MyHuniSearchController.$inject = ['$scope', '$http', '$timeout'];
/* 
 *   NetworkController 
*/
function NetworkController($scope, $routeParams, $http, $location, 
    solrSearchService, contentFormatService) {

    $scope.init = function() {
        // populate the info about our focus record
        $scope.record = $scope.recordInformation($routeParams.record);

        var config = {};
        config.params = {
            'name': $routeParams.record
        }
        // assemble list of targets connected from this source

        // get publication counts
        $scope.publications = [];
        $http.get('/vlab/entity', config)
            .then(function(response) {
                for (var i = 0; i < response.data.publications.length; i++) {
                    var uuid = response.data.publications[i];
                    $scope.getPublicationDetails(uuid);
                }
            });
        // populate the link count - src and tgt - for each entity
        $scope.src_links = [];
        $scope.tgt_links = [];
        $http.get('/vlab/link', config)
            .then(function(response) {
                for (var i = 0; i < response.data.src_links.length; i++) {
                    var data = response.data.src_links[i];
                    $scope.src_links.push({ 
                        'uuid': data.uuid, 
                        'data': $scope.recordInformation(data.tgt) ,
                        'assertion': data.assertion
                    });
                }
                
                for (var i = 0; i < response.data.tgt_links.length; i++) {
                    var data = response.data.tgt_links[i];
                    $scope.tgt_links.push({ 
                        'uuid': data.uuid, 
                        'data': $scope.recordInformation(data.src), 
                        'assertion': data.assertion 
                    });
                }
            });

            // assemble the graph
            var config = {};
            config.params = {
                'name': $routeParams.record
            };
            $http.get('/vlab/network', config)
                .then(function(response) {
                    console.log($scope.$eval(response.data.graph));
                    drawGraph($routeParams.record, $scope.$eval(response.data.graph));
                });

            // populate the details pane with the details of the focus node
            $scope.node_data = $scope.recordInformation($routeParams.record);

    }

    // flesh out the details of a publication 
    //  as well the records referenced by the publication
    $scope.getPublicationDetails = function(uuid) {
        var config = {};
        config.params = {
            'uuid': uuid
        };
        $http.get('/vlab/publication', config)
            .then(function(response) {
                    var data = {
                        'uuid': response.config.params['uuid'],
                        'title': response.data.publication.title,
                        'url': '/#/publication/' + response.config.params['uuid'],
                        'related': []
                    };
                    for (var j=0; j < response.data.publication.members.length; j++) {
                        data.related.push($scope.recordInformation(response.data.publication.members[j]));
                    }
                    $scope.publications.push(data);
            });
    }

    $scope.publicationInformation = function(pub_id) {
        var config = {};
        config.params = {
            'uuid': pub_id
        };
        return $http.get('/vlab/publication', config)
            .then(function(response) {
                var record = {};
                record.id = pub_id;
                record.type = "Publication";
                record.source = "/#/publication/" + pub_id;
                record.site = 'Humanities Network Infrastructure Project';
                record.name = response.data.publication.title;
                record.network = '';
                return record;
            });
            
    }

    // get member data
    $scope.recordInformation = function(record_id) {
        return solrSearchService.getRecordData(record_id) 
            .then(function(response) {
                var record = {};
                var data = response[0];
                record.id = data.docid;
                record.type = data.type;
                record.source = data.prov_source;
                record.site = data.prov_site_long;
                record.network = '/#/network/' + data.docid;
                if (record.type === "Person") {
                    record.name = data.given_name + ' ' + data.family_name;
                } else if (record.type === ("Bibliography")) {
                    record.name = data.title.join();
                } else if (record.type === ("Film")) {
                    record.name = data.title.join();
                } else {
                    record.name = data.name;
                }
                return record;
            });
    }

    var drawGraph = function(record_id, data) {
        var nodes = data['nodes'];
        var links = data['links'];

        var width = window.innerWidth - 100,
            height = window.innerHeight * 0.8;

        var color = d3.scale.category20();

        var force = d3.layout.force()
            .charge(-800)
            .linkDistance(150)
            .linkStrength(1)
            .size([width, height]);

        // http://stackoverflow.com/questions/7871425/is-there-a-way-to-zoom-into-a-graph-layout-done-using-d3
        // http://stackoverflow.com/questions/12310024/fast-and-responsive-interactive-charts-graphs-svg-canvas-other
        d3.select('svg').remove();
        var svg = d3.select("#graph").append("svg")
            .attr("width", width)
            .attr("height", height);
            //.append("g")
            //.call(d3.behavior.zoom().scaleExtent([0.5, 8]).on("zoom", function() {
            //    svg.attr("transform", 
            //        "translate(" + d3.event.translate + ")"
            //              + " scale(" + d3.event.scale + ")");
            //}));

        force
            .nodes(nodes)
            .links(links)
            .start();

        var link = svg.selectAll(".link")
            .data(links)
            .enter()
            .append("line")
            .attr("class", function(d) {
                if (d.type === 'pub_connection') {
                    return "link_pub";
                } else {
                    return "link";
                }
            })
            .style("stroke-width", function(d) { return Math.sqrt(d.value); });

        var circle = svg.append("g")
            .selectAll("circle")
            .data(nodes)
            .enter()
            .append("circle")
            .attr("r", function(d) {
                if (d.id === record_id) {
                    return 40;
                } else {
                    return 10;
                }
            })
            .style("fill", function(d) { return color(d.type); })
            .call(force.drag);

        circle.on("click", function(d) {
            if ( d.type === 'Publication' ) {
                $scope.node_data =  $scope.publicationInformation(d.id);
                $scope.$apply();
            } else {
                $scope.node_data = $scope.recordInformation(d.id);
                $scope.$apply();
            }
        });

        var text = svg.append("g")
            .selectAll("g")
            .data(nodes)
            .enter()
            .append("g");

        text.append("text")
            .attr("x", 15)
            .attr("y", ".35em")
            .text(function(d) { return d.type; });

        force.on("tick", function() {
            link.attr("x1", function(d) { return d.source.x; })
                .attr("y1", function(d) { return d.source.y; })
                .attr("x2", function(d) { return d.target.x; })
                .attr("y2", function(d) { return d.target.y; });

            circle.attr("transform", transform);

            text.attr("transform", transform);
          });

        var transform = function(d) {
            return "translate(" + d.x + "," + d.y + ")";
        }
   }

}
NetworkController.$inject = ['$scope', '$routeParams', '$http', '$location', 
    'SolrSearchService', 'ContentFormatService'];

/* 
 *   PublicationController 
*/
function PublicationController($scope, $routeParams, $http, $location, 
    solrSearchService, contentFormatService) {

    $scope.init = function() {
        // get the Publication data if public
        var config = {};
        config.params = {
            'uuid': $routeParams.uuid
        }
        $http.get('/vlab/publication', config)
            .success(function(response) {   
                console.log(response);
                try {
                    response.publication.synopsis = contentFormatService.formatIt(response.publication.synopsis);
                } catch (e) {
                    // skip the error - no synopsis added
                }
                try {
                    response.publication.notes = contentFormatService.formatIt(response.publication.notes);
                } catch (e) {
                    // skip the error - no notes added
                }
                $scope.data = response.publication;
                $scope.data.host = $location.host();
                $scope.data.url = $location.absUrl();
                console.log($scope.data);
                $scope.getMemberData();
            })
            .error(function(response) {
                // unauthorized
            });
    }

    // get member data
    $scope.getMemberData = function() {
        $scope.records = [];
        var members = $scope.data.members;
        for (var i = 0; i < members.length ; i++) {
            solrSearchService.getRecordData(members[i])
                .then(function(response) {
                    var data = response[0];
                    $scope.records.push({ 'source': data.prov_source, 'id': data.docid });
                    //console.log($scope.records);
                });
        }
    }

    // get list of publications available - populates
    //  the publications page
    $scope.getPublicationList = function() {
        $http.get('/vlab/publication')
            .then(function(response) {
                $scope.data = response.data.publications;
            });
    }
}
PublicationController.$inject = ['$scope', '$routeParams', '$http', '$location', 
    'SolrSearchService', 'ContentFormatService'];

/*
 *   ResultsController 
*/
function ResultsController($scope, $location, $anchorScroll, $http, 
    $timeout, solrSearchService, huniOntology) {

    // initialise the controller
    $scope.init = function() {
        // initialisation
        $scope.providers_search = [];
        $scope.types_search = [];
        $scope.applied_type_filters = [];
        $scope.applied_provider_filters = [];
        $scope.documentsToSave = [];

        // initialises linked tech state
        $scope.resetConnection();

        // do the initial data load
        $scope.results = solrSearchService.getResultSet();
        $scope.query = solrSearchService.getQuery();

        // set the result pane width to span8 initially
        $scope.controls = true;

        // this is effectively a page reload
        if ($scope.query === undefined) {
            $location.path('/');
        }

        // the list of fields to query
        $scope.query_fields = [ 'text', 'text_rev' ];

        try {
            constructQuery();
        } catch (TypeError) {
            $location.path('/');
        }
    };

    // Handle search result updates
    $scope.$on('searchResultsUpdated', function() {
        $scope.results = solrSearchService.getResultSet();
        $scope.query = solrSearchService.getQuery();
        addConnectionData();
        //$location.hash('top');
        //$anchorScroll();
        //console.log($scope.search);
    });

    // Handle facet updates
    $scope.$on('facetResultsUpdated', function() {
        var facets = solrSearchService.getFacetResults();
        //console.log(facets);

        // filter out the applied types
        var t = setSelected($scope.applied_type_filters, facets.type);
        $scope.types = t;
        var p = setSelected($scope.applied_provider_filters, facets.prov_site_long);
        $scope.providers = p;
    });

    /*
     * If a filter has been applied, set it's selected flag to true
     *
     * For example: 
     * $scope.applied_type_filters = [ 'Artefact' ]
     *
     * For facets.type.facet === 'Artefact'
     *   set facets.type.selected === true
     *
     */
    var setSelected = function(applied_filters, data) {
        // with no filters applied, return the original set
        if (applied_filters == undefined || applied_filters.length == 0) {
            return data.sort()
        }

        // see if any of the facet results are in the applied list
        // if so - set their selected flag to true
        for (var i = 0 ; i < data.length ; i ++) {
            if (applied_filters.indexOf(data[i].facet) !== -1) {
                data[i].selected = true;
            }
        }
        return data.sort();
    }

    var addConnectionData = function() {
        var docs = $scope.results.docs;
        for (var i = 0; i < docs.length; i++) {
            var docid = docs[i].docid;
            
            // get publication counts
            var config = {};
            config.params = {
                'name': docid
            }
            // populate the pub count for each entity
            $http.get('/vlab/entity', config)
                .then(function(response) {
                    var docid = response.config.params['name'];
                    for (var i=0; i < $scope.results.docs.length; i++) {
                        if ($scope.results.docs[i].docid === docid) {
                            $scope.results.docs[i].pub_count = response.data.publications.length;
                        }
                    }
                });
            // populate the link count - src and tgt - for each entity
            $http.get('/vlab/link', config)
                .then(function(response) {
                    var docid = response.config.params['name'];
                    for (var i=0; i < $scope.results.docs.length; i++) {
                        if ($scope.results.docs[i].docid === docid) {
                            $scope.results.docs[i].src_link_count = response.data.src_links.length;
                            $scope.results.docs[i].tgt_link_count = response.data.tgt_links.length;
                        }
                    }
                });
        }
    }

    $scope.toggleProvenance = function() {
        $scope.show_provenance = ! $scope.show_provenance;
    }

    // handle the assertion of a connection
    $scope.addConnection = function(record_id, type) {
        var uncheck = false;
        for (var i = 0 ; i < $scope.connection_set.length; i++) {
            if ($scope.connection_set[i] === record_id) {
                $scope.connection_set.splice(i, 1);
                uncheck = true;
            } 
        }
        // if the incoming id is in the set, then we need to remove it
        // this is the checkbox getting unchecked
        if (uncheck) {
            // unset whichever it was that was unchecked
            if ($scope.src_entity === record_id){
                $scope.src_entity = null;
                $scope.src_type = null;
            }
            if ($scope.tgt_entity === record_id){
                $scope.tgt_entity = null;
                $scope.tgt_type = null;
            }
            // modify the button label accordingly
            if ($scope.src_entity !== null && $scope.tgt_entity === null) {
                $scope.connection_button_label = "Tag target entity for connection";
            } else {
                $scope.connection_button_label = "Tag source entity for connection";
            }

        // when src == null, set it
        } else if ($scope.src_entity === null) {
            $scope.src_entity = record_id;
            $scope.src_type = type;
            $scope.connection_button_label = "Tag target entity for connection";
            $scope.controls = false;
            $scope.connection_set.push(record_id);
        // when tgt == null, set it
        } else if ($scope.tgt_entity === null) {
            $scope.tgt_entity = record_id;
            $scope.tgt_type = type;
            $scope.connection_button_label = "You can now assert the connection";
            $scope.connection_set.push(record_id);
        }
    }

    $scope.resetConnection = function() {
        try {
            for (var i = 0; i < $scope.connection_set.length; i++) {
                var e = $scope.connection_set[i] + "_connection";
                try {
                    document.getElementById(e).checked = false;
                } catch (e) {
                    // no need to do anything, it's a selection that is no longer
                    //  on the page. Consider the user has probably done a few
                    //  searches and is adding as they go.
                }
            
            }
        } catch (e) { 
            //do nothing
            //  being lazy and using this method to init the state 
            //   so connection_set is undefined on init
        }
        $scope.connection_set = [];
        $scope.src_entity = null;
        $scope.src_type = null;
        $scope.tgt_entity = null;
        $scope.tgt_type = null;
        $scope.assertion = '';
        $scope.connection_button_label = "Tag source entity for connection";
    }

    $scope.createConnection = function() {
        var data = {
            'src_entity': $scope.src_entity,
            'src_type': $scope.src_type,
            'tgt_entity': $scope.tgt_entity,
            'tgt_type': $scope.tgt_type,
            'assertion': $scope.assertion
        }
        $http.post('/vlab/mylink', data)
            .then(function(response) {
                var t = $timeout(function() { 
                    $scope.link_success_msg = undefined; 
                    $scope.resetConnection();
                    addConnectionData();
                }, 1500);
                $scope.link_success_msg = 'Connection created';
            },
            function(response) {
                var t = $timeout(function() { $scope.link_error_msg = undefined; }, 3000);
                $scope.link_error_msg = 'Unable to create connection';
            });
    }

    // handle record addition to collection
    $scope.addToCollection = function(record_id, type) {
        // iterate over the stack - if we find a match remove it and break
        // else if we get to the end - add the new member
        console.log('before');
        console.log($scope.documentsToSave);
        var found = false;
        for (var i = 0; i < $scope.documentsToSave.length ; i ++) {
            if ($scope.documentsToSave[0].record === record_id) {
                $scope.documentsToSave.splice(i, 1);
                found = true;
                break;
            }
        }
        // it wasn't already in - so add it
        if (found === false) { 
            $scope.documentsToSave.push({
                'record': record_id,
                'type': type
            });
        }
        console.log('after');
        console.log($scope.documentsToSave);

        if ($scope.documentsToSave.length > 0) {
            $scope.controls = false;
        } else {
            $scope.controls = true;
        }
    };

    // autocomplete on collection names
    $scope.autocompleteCollections = function(collection_name) {
        var config = {};
        config.params = {
            'name': collection_name
        };
        return $http.get('/vlab/ac/collection', config)
            .then(function(response) { 
                //console.log(response.data.collections);
                return response.data.collections; 
            });
    }

    // autocomplete on search names
    $scope.autocompleteSearch = function(search_name) {
        var config = {};
        config.params = {
            'name': search_name
        };
        return $http.get('/vlab/ac/search', config)
            .then(function(response) {
                //console.log(response.data);
                return response.data.searches;
            });
    }

    // save the records to the collection; in the process
    //  creating it if it doesn't exist
    $scope.saveToCollection = function() {
        $scope.collection_success_msg = undefined;
        $scope.collection_error_msg = undefined;
        data = {
            'collection_name': $scope.collection_name,
            'collection_members': $scope.documentsToSave
        };
        $http.post('/vlab/mycollection', data)
            .then(function(response) {
                //console.log($scope.documentsToSave);
                // uncheck all the checked checkboxes
                for (var i = 0; i < $scope.documentsToSave.length; i++) {
                    var e = $scope.documentsToSave[i] + "_collection";
                    try {
                        document.getElementById(e).checked = false;
                    } catch (e) {
                        // no need to do anything, it's a selection that is no longer
                        //  on the page. Consider the user has probably done a few
                        //  searches and is adding as they go.
                    }

                }
                // wipe the saved list
                $scope.documentsToSave = [];
                // wipe the collection name
                $scope.collection_name = null;

                var t = $timeout(function() { $scope.collection_success_msg = undefined; }, 1500);
                $scope.collection_success_msg = 'Collection saved';
            },
            function(response) {
                var t = $timeout(function() { $scope.collection_error_msg = undefined; }, 3000);
                $scope.collection_error_msg = 'Unable to save collection';
            });
    }

    // save the search
    $scope.saveSearch = function() {
        $scope.save_success_msg = undefined;
        $scope.save_error_msg = undefined;
        data = {
            'search_name': $scope.search_name,
            'query': $scope.query,
            'overwrite': $scope.overwrite_search 
        }
        $http.post('/vlab/mysearch', data)
            .then(function(response) {
                // wipe the search_name field
                $scope.search_name = ''
                var t = $timeout(function() { $scope.save_success_msg = undefined; }, 1500);
                $scope.save_success_msg = 'Search saved';
            },
            function(response) {
                var t = $timeout(function() { $scope.save_error_msg = undefined; }, 3000);
                if (response.status === 400) {
                    $scope.save_error_msg = 'Save failed; name already in use';
                }
            });
    }

    // get next page of results
    $scope.nextPage = function() {
        $scope.query.start = solrSearchService.NextPage($scope.results.start, $scope.results.total);
        //console.log("next: " + $scope.query.start);
        solrSearchService.doit($scope.query);
    };
    
    // get previous page of results
    $scope.previousPage = function() {
        $scope.query.start = solrSearchService.PreviousPage($scope.results.start);
        //console.log("previous: " + $scope.query.start);
        solrSearchService.doit($scope.query);
    };

    $scope.search = function() {
        //console.log('Results controller, search method');
        constructQuery();
    }

    // Add / remove a type filter from the result set
    //  filter needs to be: { field: 'foo', data: 'bar'' }
    $scope.typeFilter = function(filter) {
        // update the result set
        $scope.update($scope.types_search, $scope.applied_type_filters, filter);
    }

    // Add / remove a data provider filter from the result set
    //  filter needs to be: { field: 'foo', data: 'bar'' }
    $scope.providerFilter = function(filter) {
        // update the result set
        $scope.update($scope.providers_search, $scope.applied_provider_filters, filter);
    }

    $scope.update = function(which, applied, filter) {
        if (filter.value === true) {
            // add the provider if not already there
            which = pushStack(which, filter.data);
            applied = pushStack(applied, filter.data);
        } else {
            // remove the provider if it's in the list
            which = popStack(which, filter.data);
            applied = popStack(applied, filter.data);
        }
        constructQuery();
    }

    var pushStack = function(stack, data) {
        if (stack.indexOf(data) === -1) {
            stack.push(data);
        }
        return stack;
    }
    var popStack = function(stack, data) {
        var field_pos = stack.indexOf(data);
        if (field_pos !== -1) {
            stack.splice(field_pos,1);
        }
        return stack;
    }

    var constructQuery = function() {
        // if user_query comes in blank - set it to *
        if ($scope.query.user_query === '') { 
            $scope.query.user_query = '*';
        }
        if ($scope.query.user_query === undefined) {
            $scope.query.user_query = '*'
        }

        // our query placeholder as we're putting it all together
        var query = [];
        
        // sort out any type filters
        if ($scope.types_search.length > 0) {
            var new_query = [];
            for (var i = 0 ; i < $scope.types_search.length ; i++) {
                new_query.push({ field: 'type', data: $scope.types_search[i] });
            }
            query.push(new_query);
        }

        // sort out any data provider filters
        if ($scope.providers_search.length > 0) {
            var new_query = [];
            for (var i = 0 ; i < $scope.providers_search.length ; i++) {
                new_query.push({ field: 'prov_site_long', data: $scope.providers_search[i] });
            }
            query.push(new_query);
        }

        // sort out the query fields
        var new_query = [];
        for (var n = 0; n < $scope.query_fields.length; n++) {
            var fieldname = $scope.query_fields[n];
            new_query.push({ field: fieldname, data: $scope.query.user_query });
        }   
        query.push(new_query);

        // update the search results
        $scope.query.query = query;
        $scope.query.start = 0;
        solrSearchService.doit($scope.query);

        // update the facet counts
        var query = solrSearchService.getQuery();
        q = solrSearchService.AssembleQuery(query);
        solrSearchService.faceton(['prov_site_long', 'type'], q);

    }

}
ResultsController.$inject = ['$scope', '$location', '$anchorScroll', '$http', 
    '$timeout', 'SolrSearchService', 'HuniOntology'];

/* 
 *   SearchController 
*/
function SearchController($rootScope, $scope, $location, $routeParams,
    solrSearchService, huniOntology, userMgtService) {

    $scope.$on('facetResultsUpdated', function() {
        var facets = solrSearchService.getFacetResults();
        //console.log(facets);
        $scope.providers = facets.prov_site_long;
        $scope.nproviders = $scope.providers.length;
        $scope.entity_types = facets.type;
        $scope.nentity_types = $scope.entity_types.length;
    });

    $scope.$on('totalDocumentsUpdated', function() {
        $scope.ndocuments = solrSearchService.getTotalDocuments();
    });

    // init - get list and total, available document count
    $scope.init = function() {
        solrSearchService.faceton(['prov_site_long', 'type']);        
        solrSearchService.documentsInIndex();
        if ($routeParams.query !== undefined) {
            $scope.search($routeParams.query);
        }
    }

    // search
    $scope.search = function(query) {

        var search_request = {
            start: '0',
            result_fields: '*'
        };

        //console.log("User search: " + $scope.query);
        if ($scope.query === undefined) {
            // the user didn't type anything so search *
            $scope.query = '*';
        };
        if (query !== undefined) {
            $scope.query = query;
        }

        // when searching, the user has no concept of entity type available
        //  so, we have to effectively search all relevant fields for
        //  every entity type... /sigh
        //  
        //  Thankfully - copyfield gives us only two to look in
        var q = [];
        q.push({field: 'text', data: $scope.query});
        q.push({field: 'text_rev', data: $scope.query});
        search_request.query = [] 
        search_request.query.push(q);
        
        // and finally, put in the query string;
        search_request.user_query = $scope.query;

        //console.log("Search query:");
        //console.log(search_request);
        solrSearchService.doit(search_request);
        
        // send us to the new view then do the search
        $location.path('/results');
    }

}
SearchController.$inject = ['$rootScope', '$scope', '$location', '$routeParams',
    'SolrSearchService', 'HuniOntology', 'UserMgtService' ];

