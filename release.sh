#!/bin/bash

install_the_installer() {
cat <<EOF > pack/install.sh
#!/bin/bash

[[ \$(whoami) != 'root' ]] && echo "Run as: sudo ./install.sh" && exit 0

## install the dependencies
echo "Installing dependencies..."
aptitude install python-lxml python-argparse python-requests 2> /dev/null

## Set up the required paths
# This is a horribly repetitive and verbose way to set things up
#  but I've chosen it for the clarity it delivers
echo "Setting up the required paths..."
[[ ! -d /var/local/huni ]] && mkdir /var/local/huni
[[ ! -d /var/local/huni/harvest ]] && mkdir /var/local/huni/harvest
[[ ! -d /var/local/huni/harvest/archive ]] && mkdir /var/local/huni/harvest/archive
[[ ! -d /var/local/huni/harvest/current ]] && mkdir /var/local/huni/harvest/current

[[ ! -d /etc/huni ]] && mkdir /etc/huni
[[ ! -d /etc/huni/harvest ]] && mkdir /etc/huni/harvest
[[ ! -d /etc/huni/transforms ]] && mkdir /etc/huni/transforms

## Install the tools
echo "Installing the tools..."
cp harvester/harvest.py /usr/local/bin/

TOOLS="
    add-eaccpf-to-aggregate 
    add-paradisec-to-aggregate 
    add-other-to-aggregate 
    aggregate-rebuild 
    aggregate-update 
    analyse.py \
    clean.py 
    transform.py
"

for t in \$TOOLS; do
    cp transforms/\$t /usr/local/bin/
done

# install the default configuration
cp default-huni /etc/default/huni

## Install the transforms
echo "Installing the transforms..."
rsync -a transforms/Solr/ /etc/huni/transforms/

## Fix perms
echo "Fixing permissions..."
chown -R root. /etc/huni
chown -R www-data. /var/local/huni

echo "All done!"
EOF
chmod +x pack/install.sh
}

[[ ! -d pack ]] && rm -rf pack && mkdir pack
rsync -a harvester pack/
rsync -a transforms pack/
cat <<EOF > pack/default-huni
## HuNI tools and data locations

HARVEST="/var/local/huni/harvest/current"
TRANSFORMS="/etc/huni/transforms"
SOLR_SERVER="http://localhost:8080/solr/dev/update"
EOF
read -p 'Release number ? ' release

install_the_installer
sudo chown -R www-data. pack
mv pack pack-${release}
tar -czvf pack-${release}.tar.gz pack-${release}
sudo rm -rf pack-${release}
