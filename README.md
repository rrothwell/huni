# HuNI search and browse...


## Repository Layout
 - interface: here you'll find the code for the application itself; ie the search and browse code
 - partners/{partner project code}: the code required for that partner to provide 
a feed of their data
 - transforms/{partner project code}: the XSL transforms used on that partner data
 - harvester/{partner project code}: the harvester code for that site


