-- create huni_events view
create view huni_events as
    select
        events.eventid,
        events.event_name as event_Name, 
        events.description as description, 
        events.first_date as first_date, 
        events.last_date as last_date,
        prigenreclass.genreclass as genreclass
    from 
        events, prigenreclass
    where 
        events.primary_genre = prigenreclass.genreclassid;

-- create huni_contributors view
create view huni_contributors as
    select
        contributor.contributorid,
        contributor.first_name as first_name,
        contributor.last_name as last_name,
        contributor.date_of_birth as date_of_birth,
        contributor.date_of_death as date_of_death,
        contributor.place_of_birth as place_of_birth,
        contributor.place_of_death as place_of_death,
        country.countryname as countryname
    from 
        contributor, country
    where
        contributor.countryid = country.countryid;