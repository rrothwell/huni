#!/usr/bin/env python

import os
import os.path
import argparse
import logging
import shutil
import time

# read and check the options
parser = argparse.ArgumentParser(description='Update the repo from the cache')
parser.add_argument('--cache', required=True, dest='cache',
        help="The cache repo - the location the export script writes to.")
parser.add_argument('--repo', required=True, dest='repo',
        help="The production repo - this is the location OAI reads from.")

parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')

args = parser.parse_args()

# unless we specify otherwise
if args.debug:
    logging.basicConfig(level=logging.DEBUG)
            
if args.info:
    logging.basicConfig(level=logging.INFO)
            
if not (args.debug and args.info):
    # just give us error messages
    logging.basicConfig(level=logging.ERROR)

log = logging.getLogger('REPO_UPDATE')

if not os.path.exists(args.repo):
    os.mkdir(args.repo)

# get existing file list in repo - build a hash we can delete files from
# for each file in cache
#  if in repo - compare
#    if same - delete from repo file list
#    if different - copy new version to repo and delete from repo list
#  if not in repo
#    copy new version to repo and delete from repo list
#
# for left over files in repo hash - delete from repo

class counter:
    def __init__(self, total):
        self.last = 0
        self.total = total
        if total < 1000:
            self.logat = 100
        elif total < 100000:
            self.logat = 1000
        else:
            self.logat = 10000

    def update(self, current):
        if current > (self.last + self.logat):
            self.last += self.logat
            try:
                process_time = time.time() - self.last_time
                log.info("Processed %s of %s... (%s records in %1.2f seconds)" % (self.last, self.total, self.logat, float(process_time)))
            except:
                log.info("Processed %s of %s..." % (self.last, self.total))

def copy_file(new, old, msg):
    try:
        shutil.copy(new_file_full_path, args.repo)
        log.debug("%s: %s" % (msg, new_file_full_path))
        return True
    except:
        return False

## create the hash of existing files
log.info("Constructing the list of files in the OAI repo.")

for (dirpath, dirnames, filenames) in os.walk(args.repo):
    old_set = dict((fname, "%s/%s" % (dirpath, fname)) for fname in filenames)

## create the hash of new files in the cache
log.info("Constructing the list of new files in the cache.")
new_set = {}
for (dirpath, dirnames, filenames) in os.walk(args.cache):
    items = dict((fname, "%s/%s" % (dirpath, fname)) for fname in filenames)
    new_set = dict(new_set.items() + items.items())

## go through the new list and update any repo copies that have changed
log.info("Updating the repo.")
i = 0
c = counter(len(new_set))
for new_file_short_name, new_file_full_path in new_set.items():

    # if in the old set, copy to repo and remove from the list
    if old_set.has_key(new_file_short_name):
        copy_file(new_file_full_path, args.repo, 'File updated')
        del old_set[new_file_short_name]
             
    # otherwise - just copy it over - it's new
    else:
        copy_file(new_file_full_path, args.repo, 'New File')

    i += 1
    c.update(i)

## anything left over is a file that is no longer part of the set
##  so go through and remove it
for old_file_name, old_file_full_path in old_set.items():
    log.debug("Removing old file %s." % old_file_full_path)
    os.remove(old_file_full_path)


log.info("Total files (in cache %s) processed: %d" % (args.cache, i))

