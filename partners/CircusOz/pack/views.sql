-- create a view of the videos table using only the entries that are classified
--   public (access_level = 0) in the clips tables;
create view huni_videos as 
    select 
        videos.*, 
        clips.title as clip_title, 
        clips.description as clip_description, 
        clips.tags as clip_tags 
    from 
        videos, clips
    where 
        videos.id = clips.video_id and 
        clips.clip_type = 'video' and
        access_level = 0;

-- create an acts view joining on acttypes and acttype_clip
create view huni_acts as 
    select 
        clips.id, 
        clips.clip_type, 
        clips.start_time, 
        clips.end_time, 
        clips.title as clip_title, 
        clips.description as clip_description, 
        clips.tags as clip_tags,
        videos.title as video_title, 
        videos.city, 
        videos.country, 
        videos.start_date, 
        videos.end_date, 
        videos.venue_type, 
        videos.venue_name,
        videos.producer, 
        videos.notes,
        acttypes.name, 
        acttypes.alternative_names
    from 
        clips, acttypes, acttype_clip, videos
    where 
        acttype_clip.clip_id = clips.id and
        acttype_clip.acttype_id = acttypes.id and 
        clips.clip_type = 'act' and
        videos.id = clips.video_id and
        clips.access_level = 0;

-- create a view of the performers
create view huni_performers as
    select 
        people.id, 
        people.first_name, 
        people.last_name, 
        clips.id as clip_id, 
        personclipdata.primary_role, 
        personclipdata.secondary_role
    from 
        clips, people, personclipdata
    where 
        personclipdata.clip_id = clips.id and
        personclipdata.person_id = people.id and
        clips.access_level = 0;

-- create a view of the collections  
create view huni_collections as
    select
        collections.id as collection_id,
        collections.title as collection_title, 
        collections.description as collection_description, 
        clips.*
    from 
        collections, clips, clips_collections
    where 
        clips_collections.clip_id = clips.id and
        clips_collections.collection_id = collections.id and 
        collections.private = 0;





