# Setting up a partner for a feed

These directories contain all of the code required at a partner
site to publish a feed of their dataset.

The basic process for most sites is:

1. dump the content as XML to a staging (or cache if you will), folder

    At each site there will likely be a script which dumps the relevant data out as
    XML content. Using Bonza as an example, that script is **bonza-huni-exporter.py**.

2. update the production repository folder from the new cache content

    Once the export to XML is complete (typically to a local cache folder), a process
    is run to update the actual repository folder; in this case the script **repo-updater.py**
    is the one to run (and it's useable for all sites).

3. update the resources file for the (potentially) new repository content.

    The resources.xml file for the public repository is updated using 
    **publish-resources.py**.



## Resources publisher - publish-resources.py

A helper script to produce a resources.xml file for a site. This is the
file the harvester retrieves in order to determine what to do. The resources
file will end up in the same location as the datafiles. 

This script expects to be called with at least one param:
  --path: the path to the folder of XML files being published by the site

There is an optional third param, --exclude, which you can use to specify a list
of files in the target path you don't want listed in the available resources. Note:
only use the filenames in the exclude list, the script will take care of adding
the path to the actual file.

### Algorithm

The script iterates over all files in the directory looking for files that end
with '.xml'. For each of those, the md5 hash is calculated and a resources.xml
file is constructed with records like the following:

    <record>
        <name>E000336.xml</name>
        <hash>a21fa88a39356dcb67f3fb9401956faa</hash>
    </record>

### Example invocation

This is how you might wish to run the script for the SAUL site via cron.

    publish-resources.py --path /srv/ha/web/SAUL/LIVE/eac --exclude resources,resources.xml,publish-ohrm.py

If you're after more info about what the script is doing, try adding --info, or even --debug.

## Repository updater - repo-updater.py

A helper script to update a repository of datafiles from a cache directory used as a
primary dumping ground. Consider a process to extract a partners data first writes the raw
datafiles out to a cache - then this script updates the production repository.

### Example invocation

This is how you might wish to run the script for the SAUL site via cron.

    repo-updater.py --path /srv/ha/web/SAUL/LIVE/cache --repo /srv/ha/web/SAUL/LIVE/eac

If you're after more info about what the script is doing, try adding --info, or even --debug.

