# Publish 13 OHRMs 

In this context we mean create an up to date resources.xml file for all 13.

    root@dev01:~# time bash -x /etc/cron.daily/ohrm-republish 
    + [[ ! -f /usr/local/bin/publish-resources.py ]]
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/SAUL/LIVE/eac
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/EOAS/OAI
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/AWAP/OAInew
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/WALL/LIVE/compendium/eac
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/FACP/LIVE/vic/eac
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/FACP/LIVE/wa/eac
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/FACP/LIVE/sa/eac
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/FACP/LIVE/nsw/eac
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/FACP/LIVE/tas/eac
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/FACP/LIVE/qld/eac
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/FACP/LIVE/nt/eac
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/FACP/LIVE/australia/eac
    + /usr/local/bin/publish-resources.py --path /srv/ha/web/FACP/LIVE/act/eac

    real    1m29.532s
    user    0m4.228s
    sys     0m4.060s
