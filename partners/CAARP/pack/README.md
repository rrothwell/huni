# Timings

## Database dump to XML
    > time ./caarp-huni-exporter.py --config caarp.conf --info
    /usr/lib/python2.6/dist-packages/sqlalchemy/dialects/mysql/base.py:2111: SAWarning: Did not recognize type 'point' of column 'venue_location_point'
        self._parse_column(line, state)
    INFO:CAARP:Writing: company
    INFO:CAARP:Processed 1000 of 1760...
    INFO:CAARP:Writing: film
    INFO:CAARP:Processed 1000 of 10348... (1000 records in 0.88 seconds)
    INFO:CAARP:Processed 2000 of 10348... (1000 records in 0.42 seconds)
    INFO:CAARP:Processed 3000 of 10348... (1000 records in 0.40 seconds)
    INFO:CAARP:Processed 4000 of 10348... (1000 records in 0.41 seconds)
    INFO:CAARP:Processed 5000 of 10348... (1000 records in 0.37 seconds)
    INFO:CAARP:Processed 6000 of 10348... (1000 records in 0.38 seconds)
    INFO:CAARP:Processed 7000 of 10348... (1000 records in 0.40 seconds)
    INFO:CAARP:Processed 8000 of 10348... (1000 records in 0.41 seconds)
    INFO:CAARP:Processed 9000 of 10348... (1000 records in 0.40 seconds)
    INFO:CAARP:Processed 10000 of 10348... (1000 records in 0.40 seconds)
    INFO:CAARP:Ingesting 'film_title' data in to 'film'
    INFO:CAARP:Processed 1000 of 10348... (1000 records in 7.96 seconds)
    INFO:CAARP:Processed 2000 of 10348... (1000 records in 7.54 seconds)
    INFO:CAARP:Processed 3000 of 10348... (1000 records in 7.45 seconds)
    INFO:CAARP:Processed 4000 of 10348... (1000 records in 7.74 seconds)
    INFO:CAARP:Processed 5000 of 10348... (1000 records in 7.43 seconds)
    INFO:CAARP:Processed 6000 of 10348... (1000 records in 7.47 seconds)
    INFO:CAARP:Processed 7000 of 10348... (1000 records in 7.41 seconds)
    INFO:CAARP:Processed 8000 of 10348... (1000 records in 7.26 seconds)
    INFO:CAARP:Processed 9000 of 10348... (1000 records in 7.46 seconds)
    INFO:CAARP:Processed 10000 of 10348... (1000 records in 7.56 seconds)
    INFO:CAARP:Writing: venue
    INFO:CAARP:Processed 1000 of 1979... (1000 records in 3.02 seconds)
    INFO:CAARP:Ingesting 'venue_attributes' data in to 'venue'
    INFO:CAARP:Processed 1000 of 1979... (1000 records in 6.55 seconds)
    INFO:CAARP:Ingesting 'venue_operation_dates' data in to 'venue'
    INFO:CAARP:Processed 1000 of 1979... (1000 records in 8.31 seconds)

    real  1m41.190s
    user  0m37.030s
    sys   0m6.928s

### File count in cache directory
    > find /home/mlarosa/huni/caarp/ -type f | wc -l
    14087

### Update of repo from cache
    > time ./repo-updater.py --cache ~/huni/caarp --repo ~/huni/repo/caarp --info
    INFO:REPO_UPDATE:Constructing the list of files in the OAI repo.
    INFO:REPO_UPDATE:Constructing the list of new files in the cache.
    INFO:REPO_UPDATE:Updating the repo.
    INFO:REPO_UPDATE:Processed 1000 of 14087...
    INFO:REPO_UPDATE:Processed 2000 of 14087...
    INFO:REPO_UPDATE:Processed 3000 of 14087...
    INFO:REPO_UPDATE:Processed 4000 of 14087...
    INFO:REPO_UPDATE:Processed 5000 of 14087...
    INFO:REPO_UPDATE:Processed 6000 of 14087...
    INFO:REPO_UPDATE:Processed 7000 of 14087...
    INFO:REPO_UPDATE:Processed 8000 of 14087...
    INFO:REPO_UPDATE:Processed 9000 of 14087...
    INFO:REPO_UPDATE:Processed 10000 of 14087...
    INFO:REPO_UPDATE:Processed 11000 of 14087...
    INFO:REPO_UPDATE:Processed 12000 of 14087...
    INFO:REPO_UPDATE:Processed 13000 of 14087...
    INFO:REPO_UPDATE:Processed 14000 of 14087...
    INFO:REPO_UPDATE:Total files (in cache /home/mlarosa/huni/caarp) processed: 14087

    real    0m3.777s
    user    0m1.832s
    sys     0m1.928s

### Filecount comparison between repo and cache
    > find /home/mlarosa/huni/caarp -type f | wc -l
    14087

    > find /home/mlarosa/huni/repo/caarp -type f | wc -l
    14088
    (resources.xml is the extra file)

## Publisher script - creation of resources.xml
    > time ./publish-resources.py --path /home/mlarosa/huni/repo/caarp/ --info
    INFO:PUBLISHER:Processed 1000 of 14088...
    INFO:PUBLISHER:Processed 2000 of 14088...
    INFO:PUBLISHER:Processed 3000 of 14088...
    INFO:PUBLISHER:Processed 4000 of 14088...
    INFO:PUBLISHER:Processed 5000 of 14088...
    INFO:PUBLISHER:Processed 6000 of 14088...
    INFO:PUBLISHER:Processed 7000 of 14088...
    INFO:PUBLISHER:Processed 8000 of 14088...
    INFO:PUBLISHER:Processed 9000 of 14088...
    INFO:PUBLISHER:Processed 10000 of 14088...
    INFO:PUBLISHER:Processed 11000 of 14088...
    INFO:PUBLISHER:Processed 12000 of 14088...
    INFO:PUBLISHER:Processed 13000 of 14088...
    INFO:PUBLISHER:Processed 14000 of 14088...

    real    0m2.128s
    user    0m1.416s
    sys     0m0.708s
