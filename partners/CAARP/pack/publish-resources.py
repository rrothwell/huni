#!/usr/bin/env python

from lxml import etree
import sys
import os
import argparse
import logging
import hashlib

class counter:
    def __init__(self, total):
        self.last = 0
        self.total = total
        if total < 1000:
            self.logat = 100
        elif total < 100000:
            self.logat = 1000
        else:
            self.logat = 10000

    def update(self, current):
        if current > (self.last + self.logat):
            self.last += self.logat
            try:
                process_time = time.time() - self.last_time
                log.info("Processed %s of %s... (%s records in %1.2f seconds)" % (self.last, self.total, self.logat, float(process_time)))
            except:
                log.info("Processed %s of %s..." % (self.last, self.total))

class Publish:
    """Iterate over a directory of XML content and produce a resources.xml file"""

    def __init__(self, path, excludes):
        self.path = path
        self.excludes = excludes

    def get_hash(self, fname):
        """Given a filename, return the md5 hash"""
        with open(fname, 'r') as fh:
            try:
                m = hashlib.md5()
                data = fh.read()
                m.update(data)
                return m.hexdigest()
            except:
                log.error('Error in the has function :: OHRM code')
                log.debug(sys.exc_info())

    def update(self):
        "Update the resources file"""
        i = 0
        files = [name for name in os.listdir(self.path) if os.path.isfile(os.path.join(self.path, name))]
        files = list(set(files) - set(self.excludes))
        total = len(files)

        root = etree.Element('resources', recordCount=str(len(files)))

        i = 0
        c = counter(total)
        for f in files:
            i += 1
            c.update(i)
            if f.endswith('.xml'):
                record = etree.SubElement(root, 'record')
                name = etree.SubElement(record, 'name')
                name.text = str(f)
                filehash = etree.SubElement(record, 'hash')
                filehash.text = self.get_hash(os.path.join(self.path, f))
                log.debug(etree.tostring(record, pretty_print=True))

        
        log.debug(etree.tostring(root, pretty_print=True))

        f = open(os.path.join(self.path, 'resources.xml'), 'w')
        f.write(etree.tostring(root, pretty_print=True))
        f.close()



if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='Publish a dataset.')
    parser.add_argument('--repo', required=True, dest='path', help="The path to the datafiles.\
    It is assumed that this is the web accessible directory from which the content will be serverd. \
    Accordingly, the resources.xml file will be created here.")
    parser.add_argument('--exclude', dest='excludes', help="A comma separated list of filenames to exclude from the path")

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
            
    if args.info:
        logging.basicConfig(level=logging.INFO)
            
    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    log = logging.getLogger('PUBLISHER')

    # handle the exclude list (if any) in an appropriate manner
    if args.excludes is None:
        args.excludes = []
    else:
        args.excludes = [name for name in args.excludes.split(',')] 

    p = Publish(args.path, args.excludes)
    p.update()




